const config = require('./config/config');
const Sequelize = require('sequelize');
const sequelize= new Sequelize(
    config.development.database,
    config.development.username,
    config.development.password,
    {
        host: config.development.host,
        dialect: config.development.dialect,

        define: {
            timestamps: true
        },

        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        },

        logging: false,
        sync: {
            force: true
        }
    }
);

const db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;

//Models/tables
db.User = require('./models/User')(sequelize, Sequelize);
db.UserRole = require('./models/userrole')(sequelize, Sequelize);
db.Category = require('./models/category')(sequelize, Sequelize);
db.Delivary = require('./models/delivery')(sequelize, Sequelize);
db.OrderState = require('./models/orderstate')(sequelize, Sequelize);
db.OrderType = require('./models/ordertype')(sequelize, Sequelize);
db.SlipType = require('./models/sliptype')(sequelize, Sequelize);
db.OrderPosition = require('./models/orderposition')(sequelize, Sequelize);
db.Order = require('./models/order')(sequelize, Sequelize);
db.Article = require('./models/article')(sequelize, Sequelize);
db.Shop = require('./models/shop')(sequelize, Sequelize);


//Связывание моделей без импорта файлов ( то есть, чтобы в файле описания любой модели можно было
//обращаться к другим моделям без их импорта в виде require(...))
Object.keys(db).forEach(key => {
    if (db[key] && db[key].associate) {
        db[key].associate(db);
    }
});

//это второй способ установки отношений между моделями
// db.teacherRank.hasMany(db.teacher, {foreignKey: 'teacher_rank_id'});
// db.teacher.belongsTo(db.teacherRank, {foreignKey: 'teacher_rank_id'});
//sequelize.sync()

module.exports = db;
