'use strict';
module.exports = (sequelize, DataTypes) => {
  const Delivery = sequelize.define('Delivery', {
    name: DataTypes.STRING
  }, {});
  Delivery.associate = function(models) {
    Delivery.hasMany(models.Order, {
      foreignKey: 'delivery_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };
  return Delivery;
};
