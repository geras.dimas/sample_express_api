'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderState = sequelize.define('OrderState', {
    name: DataTypes.STRING
  }, {});
  OrderState.associate = function(models) {
    OrderState.hasMany(models.Order, {
      foreignKey: 'orderState_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };
  return OrderState;
};
