'use strict';
module.exports = (sequelize, DataTypes) => {
  const Article = sequelize.define('Article', {
    name: DataTypes.STRING,
    itemcode: DataTypes.STRING,
    barcode:DataTypes.STRING,
    jan: DataTypes.STRING,
    standart: DataTypes.STRING,
    qtyperunit: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    remaining: DataTypes.INTEGER,
    supplier_id: DataTypes.INTEGER
  }, {});
  Article.associate = function(models) {
    Article.hasMany(models.OrderPosition, {
      foreignKey: 'article_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };

  return Article;
};

