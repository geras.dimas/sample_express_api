'use strict';
module.exports = (sequelize, DataTypes) => {
  const Shop = sequelize.define('Shop', {
    name: DataTypes.STRING,
    code: DataTypes.INTEGER
  }, {});
  Shop.associate = function(models) {
    Shop.hasMany(models.User, {
      foreignKey: 'shop_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    }),
        Shop.hasMany(models.Article, {
          foreignKey: 'supplier_id',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          sourceKey: 'id'
        }),
      Shop.hasMany(models.Order, {
          foreignKey: 'shop_id',
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
          sourceKey: 'id'
      })

  };
  return Shop;
};
