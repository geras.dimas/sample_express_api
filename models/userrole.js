'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserRole = sequelize.define('UserRole', {
    name: DataTypes.STRING
  }, {});
  UserRole.associate = function(models) {
    UserRole.hasMany(models.User, {
      foreignKey: 'role_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };
  return UserRole;
};

