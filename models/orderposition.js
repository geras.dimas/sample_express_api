'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderPosition = sequelize.define('OrderPosition', {
    case: DataTypes.INTEGER,
    separate: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    intialprice: DataTypes.INTEGER,
    unitprice: DataTypes.INTEGER,
    article_id: DataTypes.INTEGER,
    order_id: DataTypes.INTEGER
  }, {});
  OrderPosition.associate = function(models) {
    // associations can be defined here
  };
  return OrderPosition;
};
