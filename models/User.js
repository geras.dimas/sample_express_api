'use strict';
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('users', {
        firstName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING
        },
        middleName: {
            type: DataTypes.STRING
        },
        login: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        userName: {
            type: DataTypes.STRING
        },
        role_id:{
            type: DataTypes.INTEGER
        },
        shop_id: {
            type: DataTypes.INTEGER
        },
        code: {
            type: DataTypes.INTEGER
        }

    }, {});
    User.associate = function(models) {
        User.hasMany(models.Order, {
            foreignKey: 'creator_id',
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
            sourceKey: 'id'
        })
        User.belongsTo(models.UserRole,{
            foreignKey: 'role_id'
        })
        User.belongsTo(models.Shop,{
            foreignKey: 'shop_id'
        })
    };
    return User;
    };
