'use strict';
module.exports = (sequelize, DataTypes) => {
  const SlipType = sequelize.define('SlipType', {
    name: DataTypes.STRING
  }, {});
  SlipType.associate = function(models) {
    SlipType.hasMany(models.Order, {
      foreignKey: 'slipType_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };
  return SlipType;
};
