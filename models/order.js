'use strict';
module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    orderDare: DataTypes.DATE,
    deliveryDate: DataTypes.DATE,
    orderState_id: DataTypes.INTEGER,
    orderType_id: DataTypes.INTEGER,
    category_id: DataTypes.INTEGER,
    delivery_id: DataTypes.INTEGER,
    slipType_id: DataTypes.INTEGER,
    creator_id: DataTypes.INTEGER,
    shop_id: DataTypes.INTEGER
  }, {});
  Order.associate = function(models) {
    Order.hasMany(models.OrderPosition, {
      foreignKey: 'order_id',
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
      sourceKey: 'id'
    })
  };
  return Order;
};
