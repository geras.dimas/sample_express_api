'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderType = sequelize.define('OrderType', {
    name: DataTypes.STRING
  }, {});
  OrderType.associate = function(models) {
      OrderType.hasMany(models.Order, {
        foreignKey: 'orderType_id',
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
        sourceKey: 'id'
      })
  };
  return OrderType;
};
