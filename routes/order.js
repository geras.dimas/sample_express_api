const express = require('express');
let router = express.Router();
let orderController = require('../controller/orderController')

/* GET users listing. */
router.get("/", orderController.getAll);

router.get("/:id", orderController.get);

router.post("/", orderController.create);

router.put("/:id", orderController.update);

router.delete("/:id", orderController.deleteById);


module.exports = router;
