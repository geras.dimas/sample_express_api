const express = require('express');
let router = express.Router();
let shopController = require('../controller/shopController')

/* GET users listing. */
router.get("/", shopController.getAll);

router.get("/:id", shopController.get);

router.post("/", shopController.create);

router.put("/:id", shopController.update);

router.delete("/:id", shopController.deleteById);


module.exports = router;
