const express = require('express');
let router = express.Router();
let userRoleController = require('../controller/userRoleController')

/* GET users listing. */
router.get("/", userRoleController.getAll);

router.get("/:id", userRoleController.get);

router.post("/", userRoleController.create);

router.put("/:id", userRoleController.update);

router.delete("/:id", userRoleController.deleteById);


module.exports = router;
