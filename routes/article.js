const express = require('express');
let router = express.Router();
let articleController = require('../controller/articleController')

/* GET users listing. */
router.get("/", articleController.getAll);

router.get("/:id", articleController.get);

router.post("/", articleController.create);

router.put("/:id", articleController.update);

router.delete("/:id", articleController.deleteById);


module.exports = router;
