const express = require('express');
let router = express.Router();
let deliveryController = require('../controller/DeliveryController')

/* GET users listing. */
router.get("/", deliveryController.getAll);

router.get("/:id", deliveryController.get);

router.post("/", deliveryController.create);

router.put("/:id", deliveryController.update);

router.delete("/:id", deliveryController.deleteById);


module.exports = router;
