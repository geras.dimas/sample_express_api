const express = require('express');
let router = express.Router();
let orderStateController = require('../controller/orderStateController')

/* GET users listing. */
router.get("/", orderStateController.getAll);

router.get("/:id", orderStateController.get);

router.post("/", orderStateController.create);

router.put("/:id", orderStateController.update);

router.delete("/:id", orderStateController.deleteById);


module.exports = router;
