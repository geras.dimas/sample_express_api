const express = require('express');
let router = express.Router();
let orderTypeController = require('../controller/orderTypeController')

/* GET users listing. */
router.get("/", orderTypeController.getAll);

router.get("/:id", orderTypeController.get);

router.post("/", orderTypeController.create);

router.put("/:id", orderTypeController.update);

router.delete("/:id", orderTypeController.deleteById);


module.exports = router;
