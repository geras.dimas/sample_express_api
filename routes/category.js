const express = require('express');
let router = express.Router();
let categoryController = require('../controller/categoryController')

/* GET users listing. */
router.get("/", categoryController.getAll);

router.get("/:id", categoryController.get);

router.post("/", categoryController.create);

router.put("/:id", categoryController.update);

router.delete("/:id", categoryController.deleteById);


module.exports = router;
