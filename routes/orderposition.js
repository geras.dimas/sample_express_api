const express = require('express');
let router = express.Router();
let orderPositionController = require('../controller/orderPositionController')

/* GET users listing. */
router.get("/", orderPositionController.getAll);

router.get("/:id", orderPositionController.get);

router.post("/", orderPositionController.create);

router.put("/:id", orderPositionController.update);

router.delete("/:id", orderPositionController.deleteById);


module.exports = router;
