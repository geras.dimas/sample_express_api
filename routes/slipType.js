const express = require('express');
let router = express.Router();
let slipTypeController = require('../controller/slipTypeController')

/* GET users listing. */
router.get("/", slipTypeController.getAll);

router.get("/:id", slipTypeController.get);

router.post("/", slipTypeController.create);

router.put("/:id", slipTypeController.update);

router.delete("/:id", slipTypeController.deleteById);


module.exports = router;
