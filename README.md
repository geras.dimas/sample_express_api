для запуска проекта сначала выполните npx sequelize-cli db:migrate. Это для  создания таблицы user в БД. Параметры подключения к БД лежат в config/config.js.
После этого проект можно запустить командой npm run start.
Сам проект из себя представляет пример REST-API. Поэтому  localhost:3000/ ничего возвращать не будет. 
Будуь работать следующие запросы
GET:http://localhost:3000/users - для получение списка пользователей

В ответ придет объект следующего вида:
[
    {
        "id": 2,
        "firstName": "test",
        "lastName": null,
        "middleName": null,
        "login": "test",
        "password": null,
        "email": null,
        "createdAt": "2019-12-07T02:27:35.636Z",
        "updatedAt": "2019-12-07T02:27:35.636Z"
    }
]

POST:http://localhost:3000/users  - создание пользователя. Для создания пользователя нужно отправлять запрос следующего вида:
{
  "login": "test",
  "firstName": "test"
}

GET:http://localhost:3000/users/1 - получение информации о пользователе по id. 
В ответ придет:
{
    "id": 1,
    "firstName": "test",
    "lastName": null,
    "middleName": null,
    "login": "test1",
    "password": null,
    "email": null,
    "createdAt": "2019-12-06T18:32:58.316Z",
    "updatedAt": "2019-12-07T02:31:54.387Z"
}
DELETE:http://localhost:3000/users/1 для  удаления пользователя по ид.

PUT: http://localhost:3000/users/1 - для редактирования пользователя по ид
Отправлять объект следующего вида:
{
  "login": "test1",
  "firstName": "test"
}

Для работы с запросами лучше всего ипользовать postman https://www.getpostman.com/downloads/

По работе с миграциями в sequelize https://sequelize.org/master/manual/migrations.html

само описание sequelize https://sequelize.org/master/index.html