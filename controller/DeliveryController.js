const db = require('../db');
const Delivary = db.Delivary;

function create(req, res) {
    Delivary.create(
        req.body
    )
        .then(res => {
            res.json(res);
        })
        .catch(err => res.json(err));
}
function getAll(req, res){
    console.log( Delivary)
    Delivary.findAll()
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}
function get(req, res){
    Delivary.findAll({where: { id: req.params.id }})
        .then(user => {
            res.json(user[0]);
        })
        .catch(err => res.json(err));
}
function update(req, res) {
    Delivary.update( req.body, { where: { id: req.params.id } })
        .then(updatedUser => {
            res.json(updatedUser);
        })
        .catch(err => res.json(err));
}
function deleteById(req, res) {
    Delivary.destroy({
        where: { id: req.params.id }
    })
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}

module.exports = {
    create,
    getAll,
    get,
    update,
    deleteById
};
