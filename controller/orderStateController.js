const db = require('../db');
const OrderState = db.OrderState;

function create(req, res) {
    OrderState.create(
        req.body
    )
        .then(res => {
            res.json(res);
        })
        .catch(err => res.json(err));
}
function getAll(req, res){
    console.log( OrderState)
    OrderState.findAll()
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}
function get(req, res){
    OrderState.findAll({where: { id: req.params.id }})
        .then(user => {
            res.json(user[0]);
        })
        .catch(err => res.json(err));
}
function update(req, res) {
    OrderState.update( req.body, { where: { id: req.params.id } })
        .then(updatedUser => {
            res.json(updatedUser);
        })
        .catch(err => res.json(err));
}
function deleteById(req, res) {
    OrderState.destroy({
        where: { id: req.params.id }
    })
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}

module.exports = {
    create,
    getAll,
    get,
    update,
    deleteById
};
