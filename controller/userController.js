const db = require('../db');
const User = db.User;

function create(req, res) {
    User.create(
        req.body
    )
        .then(res => {
            res.json(res);
        })
        .catch(err => res.json(err));
}
function getAll(req, res){
    console.log( User)
    User.findAll({ include:[db.UserRole, db.Shop]})
        .then(user => {
            res.json(user);
        })
        .catch(err => {
            console.log(err)
            res.json(err)});
}
function get(req, res){
    User.findAll({ include:[db.UserRole, db.Shop], where: { id: req.params.id }})
        .then(user => {
            res.json(user[0]);
        })
        .catch(err => res.json(err));
}
function update(req, res) {
    User.update( req.body, { where: { id: req.params.id } })
        .then(updatedUser => {
            res.json(updatedUser);
        })
        .catch(err => res.json(err));
}
function deleteById(req, res) {
    User.destroy({
        where: { id: req.params.id }
    })
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}

function login(req, res){
    User.findOne({include:[db.UserRole, db.Shop], where: { login: req.body.login, password: req.body.password }})
        .then(user => {
            console.log(user)
            res.json(user);
        })
        .catch(err => res.json(err));
}
module.exports = {
    create,
    getAll,
    get,
    update,
    deleteById,
    login
};
