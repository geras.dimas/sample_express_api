const db = require('../db');
const Shop = db.Shop;

function create(req, res) {
    Shop.create(
        req.body
    )
        .then(res => {
            res.json(res);
        })
        .catch(err => res.json(err));
}
function getAll(req, res){
    console.log( Shop)
    Shop.findAll()
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}
function get(req, res){
    Shop.findAll({where: { id: req.params.id }})
        .then(user => {
            res.json(user[0]);
        })
        .catch(err => res.json(err));
}
function update(req, res) {
    Shop.update( req.body, { where: { id: req.params.id } })
        .then(updatedUser => {
            res.json(updatedUser);
        })
        .catch(err => res.json(err));
}
function deleteById(req, res) {
    Shop.destroy({
        where: { id: req.params.id }
    })
        .then(user => {
            res.json(user);
        })
        .catch(err => res.json(err));
}

module.exports = {
    create,
    getAll,
    get,
    update,
    deleteById
};
