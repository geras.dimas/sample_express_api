const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const articlesRouter = require('./routes/article');
const categoryRouter = require('./routes/category');
const deliveryRouter = require('./routes/delivery');
const orderRouter = require('./routes/order');
const orderPositionRouter = require('./routes/orderposition');
const orderTypeRouter = require('./routes/orderType');
const orderStateRouter = require('./routes/orderState');
const shopRouter = require('./routes/shop');
const slipTypeRouter = require('./routes/slipType');
const userRoleRouter = require('./routes/userRole');


const bodyParser = require('body-parser');
let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());


app.use('/users', usersRouter);
app.use('/articles', articlesRouter);
app.use('/categories', categoryRouter);
app.use('/deliveries', deliveryRouter);
app.use('/orders', orderRouter);
app.use('/order-positions', orderPositionRouter);
app.use('/order-types', orderTypeRouter);
app.use('/order-states', orderStateRouter);
app.use('/shops', shopRouter);
app.use('/slip-type', slipTypeRouter);
app.use('/user-roles', userRoleRouter);



module.exports = app;
